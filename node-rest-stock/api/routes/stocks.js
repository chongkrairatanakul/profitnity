const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");

const StockStatistics = require("../models/stockStatistics");

// Handle incoming GET requests to /stocks
router.get("/", (req, res, next) => {
  StockStatistics.find()
    //.select("ticker EBITDA _id")
    //.populate("ticker", "EBITDA")
    .exec()
    .then(docs => {
      res.status(200).json({
        count: docs.length,
        StockStatistics: docs.map(doc => {
          return {
            _id: doc._id,
            ticker: doc.ticker,
            "Market Cap (intraday)": doc["Market Cap (intraday)"],
            "Enterprise Value": doc["Enterprise Value"],
            "Trailing P/E": doc["Trailing P/E"],
            "Forward P/E": doc["Forward P/E"],
            "PEG Ratio (5 yr expected)": doc["PEG Ratio (5 yr expected)"],
            "Price/Sales": doc["Price/Sales"],
            "Price/Book": doc["Price/Book"],
            "Enterprise Value/Revenue": doc["Enterprise Value/Revenue"],
            "Enterprise Value/EBITDA": doc["Enterprise Value/EBITDA"],
            "Fiscal Year Ends": doc["Fiscal Year Ends"],
            "Most Recent Quarter": doc["Most Recent Quarter"],
            "Profit Margin": doc["Profit Margin"],
            "Operating Margin": doc["Operating Margin"],
            "Return on Assets": doc["Return on Assets"],
            "Return on Equity": doc["Return on Equity"],
            Revenue: doc.Revenue,
            "Revenue Per Share": doc["Revenue Per Share"],
            "Quarterly Revenue Growth": doc["Quarterly Revenue Growth"],
            "Gross Profit": doc["Gross Profit"],
            EBITDA: doc.EBITDA,
            "Net Income Avi to Common": doc["Net Income Avi to Common"],
            "Diluted EPS": doc["Diluted EPS"],
            "Quarterly Earnings Growth": doc["Quarterly Earnings Growth"],
            "Total Cash": doc["Total Cash"],
            "Total Cash Per Share": doc["Total Cash Per Share"],
            "Total Debt": doc["Total Debt"],
            "Total Debt/Equity": doc["Total Debt/Equity"],
            "Current Ratio": doc["Current Ratio"],
            "Book Value Per Share": doc["Book Value Per Share"],
            "Operating Cash Flow": doc["Operating Cash Flow"],
            "Levered Free Cash Flow": doc["Levered Free Cash Flow"],
            "Beta (3y)": doc["Beta (3y)"],
            "% Held by Insiders": doc["% Held by Insiders"],
            "% Held by Institutions": doc["% Held by Institutions"],
            "Forward Annual Dividend Rate": doc["Forward Annual Dividend Rate"],
            "Forward Annual Dividend Yield":
              doc["Forward Annual Dividend Yield"],
            "Trailing Annual Dividend Rate":
              doc["Trailing Annual Dividend Rate"],
            "Trailing Annual Dividend Yield":
              doc["Trailing Annual Dividend Yield"],
            "5 Year Average Dividend Yield":
              doc["5 Year Average Dividend Yield"],
            "Payout Ratio": doc["Payout Ratio"],
            "EPS (TTM)": doc["EPS (TTM)"],
            "Target Est": doc["Target Est"],
            agoEPS: doc.agoEPS,
            estEPS: doc.estEPS,
            nyrEPS: doc.nyrEPS,
            agoCurrentQtrEPS: doc.agoCurrentQtrEPS,
            estCurrentQtrEPS: doc.estCurrentQtrEPS,
            agoCurrentNextQtrEPS: doc.agoCurrentNextQtrEPS,
            estCurrentNextQtrEPS: doc.estCurrentNextQtrEPS,
            past5Years: doc.past5Years,
            next5Years: doc.next5Years,
            epsSurprise: doc.epsSurprise,
            epsLatestDate: doc.epsLatestDate,
            request: {
              type: "GET",
              url: "http://localhost:3000/stocks/" + doc.ticker
            }
          };
        })
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.get("/:ticker", (req, res, next) => {
  console.log(req.params.ticker);
  StockStatistics.findOne({ ticker: req.params.ticker })
    //.populate("stocks")
    .exec()
    .then(stock => {
      if (!stock) {
        res.status(404).json({ message: "Stock not found" });
      }
      res.status(200).json({
        StockStatistics: stock,
        request: {
          type: "GET",
          url: "http://localhost:3000/stocks/"
        }
      });
    })
    .catch(err => {
      res.status(500).json({ error: err });
    });
});
module.exports = router;
